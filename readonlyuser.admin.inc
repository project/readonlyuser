<?php

/**
 * @file
 * Admin page callbacks for the ReadOnlyUser module.
 */

/**
 * Form builder; Configure readonlyuser settings.
 *
 * @see system_settings_form()
 */
function readonlyuser_admin_settings_form($form, $form_state) {
  $form['readonlyuser_exclude_forms'] = array(
    '#type' => 'textarea',
    '#title' => t('Do not make ReadOnly for the following forms'),
    '#default_value' => variable_get('readonlyuser_exclude_forms', ''),
    '#description' => t("Specify Forms by using their Form IDs. Enter one Form ID per line. Example: <br>search_block_form<br>     user_profile_form"),
  );
  return system_settings_form($form);
}
