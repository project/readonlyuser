ReadOnlyUser Module
================================================================================

The ReadOnlyUser module provides the flexibility to make existing users 
as Read Only. These users will not be allowed to add/modify the content.

Features
--------------------------------------------------------------------------------

* Mark existing users as Read Only
* Configuration to Avoid Read Only behaviour on specific forms
* Permission which allows other user roles to mark user as Read Only

Issues
--------------------------------------------------------------------------------

If you have issues with this module you are welcome to submit issues 
at http://drupal.org/project/readonlyuser.
