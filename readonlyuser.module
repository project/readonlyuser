<?php

/**
 * @file
 * Provides custom and override features for readonlyuser module.
 */

/**
 * Implements hook_permission().
 */
function readonlyuser_permission() {
  return array(
    'administer readonlyuser' => array(
      'title' => t('Administer ReadOnlyUser'),
      'description' => t('Permission to make a user ReadOnlyUser.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function readonlyuser_menu() {
  $items = array();
  $file_path = drupal_get_path('module', 'readonlyuser');
  $items['admin/config/people/readonlyuser'] = array(
    'title' => 'ReadOnlyUser Settings',
    'description' => 'Configurations for ReadOnlyUser.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('readonlyuser_admin_settings_form'),
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer readonlyuser'),
    'file path' => $file_path,
    'file' => 'readonlyuser.admin.inc',
    'weight' => 1,
  );
  return $items;
}

/**
 * Implements hook_user_login().
 */
function readonlyuser_user_login(&$edit, $account) {
  $_SESSION['readonlyuser'] = readonlyuser_db_operation('fetch', array('uid' => $account->uid));
}

/**
 * Implements hook_FORMID_alter().
 */
function readonlyuser_form_user_profile_form_alter(&$form, &$form_state) {
  if (user_access('administer readonlyuser')) {
    $form['readonlyuser'] = array(
      '#type' => 'checkbox',
      '#title' => t('ReadOnlyUser'),
      '#default_value' => readonlyuser_db_operation('fetch', array('uid' => $form['#user']->uid)),
      '#description' => t('If checked, this user cannot update any data.'),
    );
    $form['#submit'][] = 'readonlyuser_submit';
  }
}

/**
 * Custom Submit Handler.
 */
function readonlyuser_submit(&$form, &$form_state) {
  readonlyuser_db_operation('update', array('uid' => $form['#user']->uid, 'status' => $form_state['values']['readonlyuser']));
}

/**
 * Implements hook_form_alter().
 */
function readonlyuser_form_alter(&$form, &$form_state, $form_id) {
  if (user_access('administer readonlyuser')) {
    // Avoid the Chicken and the Egg Problem here.
    return;
  }
  if (isset($_SESSION['readonlyuser']) && !empty($_SESSION['readonlyuser']) && $_SESSION['readonlyuser']) {
    $forms = variable_get('readonlyuser_exclude_forms');
    $forms_array = explode("\n", $forms);
    if (!empty($forms_array)) {
      foreach ($forms_array as $form_array) {
        if ($form_array == $form_id) {
          // Exclude Read Only Behaviour for Forms in Config.
          return;
        }
      }
    }
    // Restrict access to all elements in this form.
    foreach (element_children($form) as $key) {
      $form[$key]['#access'] = FALSE;
    }
    $form['#attributes'] = array('disabled' => TRUE);
    $form['#prefix'] = t('You are a Read Only User, therefore restricted access to edit.');
  }
}

/**
 * Helper function to perform the Database Operations.
 *
 * @param string $operator
 *   String, specifying the operation to be performed.
 * @param array $data
 *   Array, related data to the operator above.
 *
 * @return int
 *   Interger, based on the operation performed.
 */
function readonlyuser_db_operation($operator, array $data = array()) {
  if ($operator == 'update' && !empty($data) && !empty($data['uid'])) {
    $return = db_merge('readonlyuser_data')
      ->key(array('uid' => $data['uid']))
      ->fields(array(
        'uid' => $data['uid'],
        'status' => $data['status'],
      ))
      ->execute();
  }
  elseif ($operator == 'fetch' && !empty($data) && !empty($data['uid'])) {
    $result = db_select('readonlyuser_data', 'rud')->fields('rud', array('uid', 'status'))->condition('uid', $data['uid'], '=')->execute()->fetchAssoc();
    if (!empty($result)) {
      return $result['status'];
    }
    return 0;
  }
}

/**
 * Implements hook_help().
 */
function readonlyuser_help($path, $arg) {
  switch ($path) {
    case 'admin/help#readonlyuser':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The ReadOnlyUser module provides the flexibility to make existing users as Read Only. These users will not be allowed to add/modify the content. <a href="!url">Project page</a> of the ReadOnlyUser module.', array('!url' => url('https://www.drupal.org/project/readonlyuser', array('external' => TRUE)))) . '</p>';
      $output .= '<h3>' . t('Main Features') . '</h3>';
      $output .= '<ul>';
      $output .= '<li>' . t('Mark existing users as Read Only') . '</li>';
      $output .= '<li>' . t('Configuration to Avoid Read Only behaviour on specific forms') . '</li>';
      $output .= '<li>' . t('Permission which allows other user roles to mark user as Read Only') . '</li>';
      $output .= '</ul>';
      return $output;
  }
}
